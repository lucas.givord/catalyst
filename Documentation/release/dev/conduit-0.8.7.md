## conduit 0.8.7

Update to conduit 0.8.7 internally. This brings in a few more APIs:

- `conduit_datatype_sizeof_index_t`
- `conduit_node_move`
- `conduit_node_reset`
- `conduit_node_swap`
