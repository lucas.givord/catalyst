## Python bindings

The Catalyst API is now wrapped in Python.
This makes also available the Python bindings of conduit.
All functions live under the `catalyst` Python module.
Input parameters can be created using conduit's Python API.
